#!/usr/bin/env sage

import sys
from attributes import *
from sage.all import *

#tento skript tvori random grafy
#doporucuji radeji vyuzit mathematicu, ktera to zvlada rychleji, ackoli je tam tesi generovat grafy danych vlastnosti


#vytvari random grafy
def createRandom(edges, nodes, count):
	#filename = 'in/' + 'e' + str(edges) + 'n' + str(nodes) + 'g' + str(group) + '.csv'
	filename = 'in/tree-' + 'n' + str(nodes) + 'g' + str(group) + '.csv'	
	with open(filename, 'w') as f:
		i = 0
		while i < count:
			#j = graphs.RandomGNM(edges, nodes)
			j = graphs.RandomTree(nodes)
			#if not math.isinf(j.radius()) :
			gr = graphs_list.to_graph6([j])
			f.write(gr)
			i += 1;
	return filename	

	
if len(sys.argv) < 3:
	print("-1")
	sys.exit(-1)



#edges = int(sys.argv[1])
#nodes = int(sys.argv[2])
#group = int(sys.argv[3])

edges = 0
nodes = int(sys.argv[1])
group = int(sys.argv[2])

#uklada do csv souboru
filename = createRandom(edges, nodes, group)

print(filename)

sys.exit(0);
