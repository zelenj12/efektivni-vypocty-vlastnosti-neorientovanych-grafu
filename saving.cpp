#include<stdio.h> 
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <sys/stat.h>
#include <sys/types.h>
#include <bits/stdc++.h>
#include <string>

// Tento soubor je urcen pouze pro ukladani vysledku a zapis do statistiky. 
// Vysledky uklada so slozky results a nasledne si tvori cestu, tak aby vysledky byly ulozeny smysluplne.


using namespace std;

/*
*Vytvari strom slozek, kam se ulozi vysledky
*argument: cest
*output: bool, zda cesta byla vytvorena
*/
bool create_dir_tree(string path) {
	bool status = false;
	string rule = "mkdir -p ";
	rule.append(path);
	char cmd[rule.size() + 1];
	strcpy(cmd, rule.c_str());
	
	
	int res = system(cmd);
	
	if(res == -1) {
		cerr << "Error: Can't create directory path to result! " << strerror(errno) << endl;
		status = false;
	} else {
		cout << "Directory path was created." << endl;
		status = true;
	}
	
	return status;
}

/*
*Vytvari strom cestu slozek, kam se ulozi vysledky
*argument: jmeno a vlastnost
*output: string, cesta k souboru
*/
string create_dir_path(char *name, char *attribute) {
	string res = "results/";
	res.append(attribute);
	res.append(1, '/');
	res.append(name);
	return res;
}

/*
*Vytvari jmeno souboru, kam se ulozi vysledky
*argument: pocet hran, pocet nodu, pocet grafu
*output: string, jmeno souboru
*/
string create_file_name(char *edge, char *node, char *count) {
	string res = "n";
	res.append(node);
	res.append(1, 'e');
	res.append(edge);
	res.append(1, 'c');
	res.append(count);
	return res;
}


int main(int argc, char *argv[]) {

	cout << "spoustim ukladani..." << endl;


	// argumenty tohoto skriptu
	char *name = argv[1];
	char *attribute = argv[2];
	char *edges = argv[3];
	char *nodes = argv[4];
	char *count = argv[5];
	
	
	string dir_path = create_dir_path(name, attribute);
	
	//pokud se nevytvori cesta pro ulozeni vysledku skonci
	if(!create_dir_tree(dir_path)) return -1;
	
	string whole_path;
	whole_path.append(dir_path);
	whole_path.append(1, '/');
	whole_path.append(create_file_name(edges, nodes, count));
	whole_path.append({'.', 'c', 's', 'v'});
	
	ofstream csvfile;
	csvfile.open (whole_path);
	
	
	string str;
	//magicky string, ktery rozlisuje zacatek validniho vypisu
	string checkstr = "--**--";
	double time;
	
	cout << "Vypis programu: " << endl;
	
	
	while (str != checkstr) {
		getline(cin, str);
		cout << str << endl;
	}
	
	cin >> time;
	if(time < 0) {
		cerr << "Error: Didnt get data!" << endl;	
		return -1; 
	}
	
	//uklada validni data do souboru csv
	csvfile << "g6,attribute,time,total - "  << time;
	
	
	int countofgotten = 0;
	
	while (getline(cin, str)) {
		cout << countofgotten++ << endl;
		csvfile << str << "\n";
	}
	
	csvfile.close();
	
	ofstream statfile;
	
	//zapisuje vysledky do statistiky
	statfile.open ("results/stat.csv", ofstream::app);
	
	statfile << name << "," << attribute << "," << nodes << "," << edges << "," << count << "," << time << endl;
	
	statfile.close();
	
	
	return 0;

}
