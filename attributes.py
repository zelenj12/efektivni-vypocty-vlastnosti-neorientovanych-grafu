#Tento soubor je urcen pro volani jednotlivych funkci pro zjistovani vlastnosti grafu
#Je vytvoren, aby se dali jednoduse pridavat dalsi vypocty vlastnosti

# Zjistuje klikovost
def cliqNumber(g):
	return g[0].clique_number() 

# Zjistuje chromaticke cislo
def chromNumber(g):
	return g[0].chromatic_number()

# Zjistuje chromaticky index
def chromIndex(g):
	return g[0].chromatic_index()


# Zjistuje perfektnost
def perfect(g):
	return g[0].is_perfect()


# Zjistuje nezavisle cislo
def independentNumber(g):
	I = IndependentSets(g[0])
	return I.cardinality()
	
# Zjistuje polomer
def getRadius(g):
	return g[0].radius()
	
# Zjistuje prumer
def getDiameter(g):
	return g[0].diameter()
	
# Zjistuje, zda je graf bipartitni
def isBipartite(g):
	return g[0].is_bipartite()
	
# Zjistuje, zda je graf stromem
def isTree(g):
	return g[0].is_tree()

# Zjistuje zda neobsahuje trojuhelniky
def isTriangleFree(g):
	return bool(not bool(g[0].triangles_count()))
	
# Zjistuje obvod
def girth(g):
	return g[0].girth()
	
# Zjistuje, zda je graf Euleriansky
def isEulerian(g):
	return g[0].is_eulerian()

# Zjistuje, zda je graf Hamiltonovsky
def isHamiltonian(g):
	return g[0].is_hamiltonian()

# Zjistuje, zda je graf rovinny
def isPlanar(g):
	return g[0].is_planar()
	
# Zjistuje vrcholovou tranzitivitu
def isVertexTransitive(g):
	return g[0].is_vertex_transitive()
	
# Zjistuje, zda je graf silne regularni
def isStronglyRegular(g):
	return g[0].is_strongly_regular()
	
# Zjistuje, zda je graf spojity
def isConnected(g):
	return g[0].is_connected()
	
# Zjistuje, zda je graf biconnetni
def isBiconnected(g):
	return g[0].is_biconnected()
	
# Zjistuje pocet mostu
def countOfBridges(g):
	return g[0].bridges()

# Zjistuje, zda je graf cactus
def isCactus(g):
	return g[0].is_cactus()
	
# Zjistuje, zda je graf regularni
def isRegular(g):
	return g[0].is_regular()
	
# Zjistuje pocet koster v grafu
def countOfSpanningTree(g):
	return g[0].spanning_trees_count()
	
# Zjistuje, zda je graf distancne regularni
def isDistanceRegular(g):
	return g[0].is_distance_regular()
	
# Zjistuje, zda je graf hranove tranzitivni
def isEdgeTransitive(g):
	return g[0].is_edge_transitive()
	
# Zjistuje, zda je graf arc-transitive
def isArcTransitive(g):
	return g[0].is_arc_transitive()	
	
	
		
