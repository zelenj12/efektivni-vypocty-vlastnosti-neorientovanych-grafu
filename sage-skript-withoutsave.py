#!/usr/bin/env sage

import sys
from attributes import *
from sage.all import *
from sage.graphs.independent_sets import IndependentSets
from sage.graphs.distances_all_pairs import diameter

#funkce pro nacitani grafu ze souboru
def loadGraphs(filename):
	with open(filename) as f:
		l = graphs_list.from_whatever(f)
	return l
	
# funkce, ktera meri rychlost vypoctu a sposti vypocet postupne na grafech
def runFunctionOnGraphs(func, list, res_list):
	for g in list:
		timeGraph = walltime()
		tmpl = [g]
		res = func(tmpl)
		roundTimeGraph = round(walltime(timeGraph), 10)
		res_list.append((res, roundTimeGraph))

#funkce uklada hodnoty do souboru ve forme csv
def saveValues(graph_list, res_list, attribute):
	print('g6,'+ str(attribute))
	#zrusit zapis grafu
	graphs = graphs_list.to_graph6(graph_list).split('\n')
	for i in range(len(graphs)-1):
		print("" + str(graphs[i]) + "," + str(res_list[i][0]) + "," + str(res_list[i][1]))


# funkce, ktera podle argumentu zjistuje danou vlastnost
# posila dale list grafu a zjistovanou vlastnost a ocekava navrat casu vypoctu do res_listu
def switchVal(arg, l, res_list):
	if arg == 'getCliqueNum':
		return runFunctionOnGraphs(cliqNumber, l, res_list)
	elif arg == 'getChromaticNum':
		return runFunctionOnGraphs(chromNumber, l, res_list)
	elif arg == 'isPerfect':
		return runFunctionOnGraphs(perfect, l, res_list)
	elif arg == 'independentNumber':
		return runFunctionOnGraphs(independentNumber, l, res_list)
	elif arg == 'getRadius':
		return runFunctionOnGraphs(getRadius, l, res_list)
	elif arg == 'getDiameter':
		return runFunctionOnGraphs(getDiameter, l, res_list)
	elif arg == 'isBipartite':
		return runFunctionOnGraphs(isBipartite, l, res_list)
	elif arg == 'isTree':
		return runFunctionOnGraphs(isTree, l, res_list)
	elif arg == 'isTriangleFree':
		return runFunctionOnGraphs(isTriangleFree, l, res_list)
	elif arg == 'isRegular':
		return runFunctionOnGraphs(isRegular, l, res_list)
	elif arg == 'Girth':
		return runFunctionOnGraphs(girth, l, res_list)
	elif arg == 'isEulerian':
		return runFunctionOnGraphs(isEulerian, l, res_list)
	elif arg == 'isHamiltonian':
		return runFunctionOnGraphs(isHamiltonian, l, res_list)
	elif arg == 'isPlanar':
		return runFunctionOnGraphs(isPlanar, l, res_list)
	elif arg == 'isStronglyRegular':
		return runFunctionOnGraphs(isStronglyRegular, l, res_list)
	elif arg == 'isConnected':
		return runFunctionOnGraphs(isConnected, l, res_list)
	elif arg == 'isBiconnected':
		return runFunctionOnGraphs(isBiconnected, l, res_list)
	elif arg == 'countOfSpanningTree':
		return runFunctionOnGraphs(countOfSpanningTree, l, res_list)
	elif arg == 'isDistanceRegular':
		return runFunctionOnGraphs(isDistanceRegular, l, res_list)
	elif arg == 'isEdgeTransitive':
		return runFunctionOnGraphs(isEdgeTransitive, l, res_list)
	else:
		print('Not supported yet')
		sys.exit(-2)


#script skonci pri nedostatecnem poctu argumentu
if len(sys.argv) < 3:
	print("-1\n")
	sys.exit(-1)

#jako argumenty tento skript bere jmeno souboru a vlastnosti, ktere ma zjistovat
filename = sys.argv[1]
att = sys.argv[2]

print("Spusten python s argumenty " + filename + " " + att)

#li = loadGraphs(filename)
li = []
with open(filename) as f:
	li = graphs_list.from_whatever(f)

#vypisuje vysledne hodnoty
print("Grafy nacteny. Nyni spoustim vypocet")

print("Loaded graphs:" + '\n' + str(len(li)))

print("--**--")


res_list = []
w = walltime()
switchVal(att, li, res_list)
print("" + str(round(walltime(w), 5)))



saveValues(li, res_list, att)


sys.exit(len(li));
