#!/bin/bash

#skript urceny ke spousteni a hledani jedne konkretni vlastnosti v jednom nebo vice souborech g6

#je nutne zadavat cestu ke slozce bez lomitka na konci!!!
mathematica_path=./math-skript-withoutsave.wls
sage_path=./sage-skript-withoutsave.py

echo Script is running...

#argumenty skriptu:

#jmeno programu
prg=$1

# hledana vlastnost
attribute=$2

# cesta k souboru nebo slozce
random=$3

#pocet hran
edges=$5

#pocet vrcholu
nodes=$6

#pocet grafu
group=$4

#kontrola, ktery program se ma spustit a nastaveni cesty k nemu
if [ "$prg" == "Mathematica" ] ; then

	program_path=$mathematica_path
	
elif [ "$prg" == "SageMath" ] ; then

	program_path=$sage_path
		
else
	
	echo Neodpovida ani Mathematice ani SageMath
	#skonci pri neznamem programu
	exit 1
	
fi

#Kontroluje, zda dostal validni cestu do nejake slozky nebo k souboru. Pokud ne generuje nove grafy
if [[ -d $random ]] ; then

	echo Budu cist postupne vsechny soubory s daty ve slozce $random
	filename=($random/*)
	echo Obsah adresare je- ${filename[*]}

elif [[ -f $random ]] ; then
	
	echo Budu cist data ze souboru $random
	filename=($random)

else

	echo Vytvarim random data pro zadane parametry.
	res=$(./skript-create-random.py $edges $nodes $group)
	filename=(`echo "${res}"`)
	echo Nahodne grafy jsou ulozeny v souboru ${filename[0]}

fi

#spousti pro vsechny soubory ve slozce
for ((i=0; i<${#filename[@]}; i++)); do

#podle jmena input souboru zjistuje vlastnosti o grafech, je proto nutne zachovat spravne nazvy
tmppred=${filename[$i]#*-e}
tmpend=${tmppred%.*}
tmpendc=${tmpend%c*}
tmp=(${tmpendc//n/ })
edges=${tmp[0]}
nodes=${tmp[1]}

now=$(date +"%T")

echo Spoustim $program_path a na grafech s $edges hranami a $nodes vrcholy ze souboru "${filename[$i]}" hledam vlastnost $attribute. Zacatek $now 

#spousti a meri cas, nasledne pipelinou preposila data do skriptu saving, ktery data ulozi a zapise vysledky do statistiky
exec 3>tmp.txt 4>&2
cas=$( TIMEFORMAT="%3R"; { time $program_path "${filename[$i]}" $attribute | ./saving $prg $attribute $edges $nodes $group 1>&3 2>&4; } 2>&1 )
exec 3>&- 4>&-

suma=`echo $cas | bc`

echo Potrebny cas pro vypocet: $suma vcetne ulozeni.

#uloz celkovy cas vcetne zapisu do tabulky - nutno mit spravny nazev
truncate -s-1 results/stat.csv
echo ,$suma >> results/stat.csv


done

